# Exemple applicatif springboot rest avec swagger
## Infos

* Compiler le projet : mvn package
* Afficher la doc swagger : http://localhost:8080/swagger-ui.html
* Lancer l'application : java -jar target/*.jar

## Tests

* Tester le endpoint avec secu : curl http://user1:secret1@localhost:8080/protected-greeting (affiche une popup basic authent dans le navigateur si pas de session "authorization" en cours sur le navigateur)
* Tester le endpoint ouvert : curl http://localhost:8080/greeting
* Test sans login/password :  curl http://localhost:8080/protected-greeting
  {"timestamp":"2018-09-20T09:50:47.665+0000","status":401,"error":"Unauthorized","message":"Unauthorized","path":"/protected-greeting"}
* Sur les tests navigateurs, peut etre utile, d'effacer la session basic http courante, avec Ctrl+Shift+N : https://stackoverflow.com/questions/5957822/how-to-clear-basic-authentication-details-in-chrome
