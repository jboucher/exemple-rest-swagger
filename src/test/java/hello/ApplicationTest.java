package hello;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;

@RunWith(JUnit4.class)
public class ApplicationTest {

    @Test
    public void should_run_main_with_success() throws IOException {
        Application.main(new String[] {"--spring.profiles.active=test"});
    }

    @Test
    public void test1() throws Exception {
        System.out.println("test 1");
       // throw new Exception("errooor test 1");

    }

    @Test
    public void test2() throws IOException {
        System.out.println("test 2");

    }
}
