package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// http://localhost:8080/swagger-ui.html pour voir le swagger-ui

@SpringBootApplication
@EnableSwagger2
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("Mon appli de test avec swagger")
//                .description("Ma super appli de test avec swagger que je décris ici")
//                .termsOfServiceUrl("http://www-03.ibm.com/software/sla/sladb.nsf/sla/bm?Open")
//                .contact("Actemium")
//                .license("Licence classique")
//                .licenseUrl("http://localhost/")
//                .version("1.2.0")
//                .build();
//    }
}
