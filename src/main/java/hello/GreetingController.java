package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s, username %s !";
    private final AtomicLong counter = new AtomicLong();

    // http://heidloff.net/article/usage-of-swagger-2-0-in-spring-boot-applications-to-document-apis/
    @ApiOperation(value = "getGreeting", nickname = "getGreeting", httpMethod = "GET")
    @RequestMapping("/greeting")

    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "User's name", required = false, dataType = "string", paramType = "query", defaultValue="Niklas")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Greeting.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name, "Anonyme"));
    }


    // http://heidloff.net/article/usage-of-swagger-2-0-in-spring-boot-applications-to-document-apis/
    @ApiOperation(value = "getProtectedGreeting", nickname = "getProtectedGreeting", httpMethod = "GET")
    @RequestMapping("/protected-greeting")

    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "User's name", required = false, dataType = "string", paramType = "query", defaultValue="Niklas")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Greeting.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public Greeting protectedGreeting(@RequestParam(value="name", defaultValue="World") String name, @AuthenticationPrincipal final UserDetails user) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name, user.getUsername()));
    }
}

